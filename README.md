# { JSON RPC 2.0 } for Laravel 

[![Total Downloads](https://poser.pugx.org/alex-kudrya/laravel-jsonrpc/downloads)](//packagist.org/packages/alex-kudrya/laravel-jsonrpc)
[![Version](https://poser.pugx.org/alex-kudrya/laravel-jsonrpc/version)](//packagist.org/packages/alex-kudrya/laravel-jsonrpc)
[![License](https://poser.pugx.org/alex-kudrya/laravel-jsonrpc/license)](//packagist.org/packages/alex-kudrya/laravel-jsonrpc)
[![PHP Version Require](http://poser.pugx.org/alex-kudrya/laravel-jsonrpc/require/php)](https://packagist.org/packages/alex-kudrya/laravel-jsonrpc)

What is JSON-RPC you can learn here -  **[#JSONRPC](https://www.jsonrpc.org/)**

## Installation
```
composer require alex-kudrya/laravel-jsonrpc
```

In `app.php` add to `providers` array `\AlexKudrya\Adminix\Providers\AdminixServiceProvider::class`

```php
// config/app.php

'providers' => [ 
    ... 
    \AlexKudrya\LaravelJsonRpc\Providers\JsonRpcServiceProvider::class,
    ...
 ]
```

Publish package to your app

```
php artisan vendor:publish --provider=AlexKudrya\LaravelJsonRpc\Providers\JsonRpcServiceProvider
```
## How it works
JSON-RPC API it is an Api with single endpoint, by default it is `https://your-domain.com/json-rpc/v1`, **json-rpc/v1** part can be configured in `config/json_rpc.php`. 

**Requests uses always only `POST` method.**

**There are exist fixed request - response structure for this protocol.**

### Example

**Request**
```json
{
    "jsonrpc": "2.0",
    "method": "ControllerName@MethodName",
    "params": {
        "parameter": "value"
    },
    "id": "some_unique_request_id"
}
```
**Responce**
```json
{
    "jsonrpc": "2.0",
    "result": {
        "key": "value"
    },
    "id": "some_unique_request_id"
}
```

`jsonrpc`, `method`, `params`, `id` **are required fields for JSON-RPC request**


| Field | Description |
| ------ | ------ |
|**jsonrpc**|Just always equal to **2.0**, it is constant, dont think about it.|
|**method**|The method we calling to.|
|**params**|JSON with input data, can be empty, but must be exist.|
|**id**|An unique request ID, need for front-end. When you use batch-requests, your front-end need to indentify which responce for which request was returned|

**Calling Method** consist from these parts:
- **Controller name**
- **Separator**
- **Method name**

Example: `User@get`\
In this example:
- **"User"** is a controller name, means `App\Http\Controllers\JsonRpc\UserController` controller/class. **App\Http\Controllers\JsonRpc** it is a `controllers_root_namespace` from `config/json_rpc.php` file, and **Controller** is a `controllers_postfix` from  `config/json_rpc.php` file.
- **"@"** is a separator, `controllers_method_delimiter` from  `config/json_rpc.php` file.
- **"get"** means we calling **get()** method from this controller.

As a result, `method` **User@get** calling **get()** method from **App\Http\Controllers\JsonRpc\UserController**


## Check Server
To check server status you need make this request:
```json
{
    "jsonrpc": "2.0",
    "method": "ping",
    "params": {},
    "id": "dlalkmd1231da"
}
```
If everythig is ok you will receive this responce:
```json
{
    "jsonrpc": "2.0",
    "result": {
        "message": "pong"
    },
    "id": "dlalkmd1231da"
}
```

## Basic usage

Request:
```json
{
    "jsonrpc": "2.0",
    "method": "Product@create",
    "params": {
        "name": "pencil",
        "price": "1.5"
    },
    "id": "lsfkkj3fl3k92202jf2n32k430f"
}
```

Controller:
```php
// app/Http/Controllers/JsonRpc/ProductController.php

namespace App\Http\Controllers\JsonRpc;

use AlexKudrya\LaravelJsonRpc\Requests\JsonRpcRequest;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function create(JsonRpcRequest $request)
    {
        $request->params(); // ['name' => 'pencil', 'price' => '1.5']
        $request->params('name'); // pencil
        $request->param('price'); // 1.5

        $request->id(); // lsfkkj3fl3k92202jf2n32k430f
        $request->method() // Product@create
        $request->method(true) // ["controller" => "Product", "method" => "create"]

        // you can use your input params like this:

        $product = Product::create([
            'name' => $request->params('name'),
            'price' => $request->params('price'),
        ])

        // or like this:

        $product = Product::create($request->params())

        return $product;
    }
}

```

Responce:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "id": "1",
        "name": "pencil",
        "price": "1.5"
    },
    "id": "lsfkkj3fl3k92202jf2n32k430f"
}
```

### Atiention
- To use request validation your Request class must extends `AlexKudrya\LaravelJsonRpc\Requests\JsonRpcRequest` class.
- Do not use `$request->all()` or `$request->input()` to get your input parameters, you need to use `$request->params()` or `$request->param({key})` for this purpose

## Batch requests
If you do not need to make several requests in parallel, you can use batch requests. The peculiarity of this type of request is that requests are executed sequentially in the order in which they are located in the request. Keep in mind that the failure of one request does not block the execution of the following requests.

You can use batch requests to send several requests in one:

Request:
```json
[
    {
        "jsonrpc": "2.0",
        "method": "Product@create",
        "params": {
            "name": "pencil",
            "price": "1.5"
        },
        "id": "lsfkkj3fl3k92202jf2n32k430f"
    },
    {
        "jsonrpc": "2.0",
        "method": "Product@create",
        "params": {
            "name": "pen",
            "price": "3.2"
        },
        "id": "dqsfkfOd2dwkdwo1231dlslawm20"
    }
]
```

Responce:

```json
[
    {
        "jsonrpc": "2.0",
        "result": {
            "id": "1",
            "name": "pencil",
            "price": "1.5"
        },
        "id": "lsfkkj3fl3k92202jf2n32k430f"
    },
    {
        "jsonrpc": "2.0",
        "result": {
            "id": "2",
            "name": "pen",
            "price": "3.2"
        },
        "id": "dqsfkfOd2dwkdwo1231dlslawm20"
    }
]
```
