<?php

namespace AlexKudrya\LaravelJsonRpc;

use JetBrains\PhpStorm\ArrayShape;

class JsonRpcResponse
{
    #[ArrayShape(['jsonrpc' => "string", 'result' => "mixed", 'id' => "null|string"])]
    public static function success(mixed $result, string $id = null): array
    {
        return [
            'jsonrpc' => JsonRpcServer::JSON_RPC_VERSION,
            'result'  => $result,
            'id'      => $id,
        ];
    }

    #[ArrayShape(['jsonrpc' => "string", 'error' => "mixed", 'id' => "null"])]
    public static function error(mixed $error, string $id = null): array
    {
        return [
            'jsonrpc' => JsonRpcServer::JSON_RPC_VERSION,
            'error'   => $error,
            'id'      => $id,
        ];
    }
}
