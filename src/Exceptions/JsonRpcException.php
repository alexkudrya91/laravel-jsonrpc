<?php

namespace AlexKudrya\LaravelJsonRpc\Exceptions;

class JsonRpcException extends \Exception
{
    /**
     * @var array
     */
    private array $additional_data = [];

    /**
     * @return array
     */
    public function getAdditionalData(): array
    {
        return $this->additional_data;
    }

    /**
     * @return array
     */
    public function export(): array
    {
        return [
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
            'data' => $this->getAdditionalData()
        ];
    }

}
