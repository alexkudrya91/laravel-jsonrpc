<?php

namespace AlexKudrya\LaravelJsonRpc\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class JsonRpcRequest extends FormRequest
{
    private array $validationData = [];

    public function setValidationData(array $validationData): void
    {
        $this->validationData = $validationData;
    }

    public function authorize(): bool
    {
        return true;
    }

    public function validationData(): array
    {
        return $this->validationData ?? [];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator);
    }

    public function method(bool $parsed = false)
    {
        if ($parsed) {
            $method = explode(config('json_rpc.controllers_method_delimiter'),$this->all()['method']);
            return [
                'controller' => $method[0],
                'method' => $method[1],
            ];
        }

        return $this->all()['method'];
    }

    public function jsonrpc()
    {
        return $this->all()['jsonrpc'];
    }

    public function id()
    {
        return $this->all()['id'];
    }

    /**
     * Get specific value from input parameters
     *
     * @param string $key
     * @param $default
     * @return mixed
     */
    public function param(string $key, $default = null)
    {
        return $this->params($key, $default);
    }

    /**
     * Get all values from input parameters, or one specific
     *
     * @param mixed|null $key
     * @param $default
     * @return mixed
     */
    public function params(mixed $key = null, $default = null)
    {
        return data_get(
            $this->params, $key, $default
        );
    }

    public function __call($name, $arguments) {
        if (in_array($name, config('json_rpc.additional_high_level_parameters'), $name)) {
            return $this->all()[$name];
        }
        return parent::__call($name, $arguments);
    }
}
