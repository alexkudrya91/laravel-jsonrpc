<?php

use AlexKudrya\LaravelJsonRpc\JsonRpcServer;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| JSON-RPC Route
|--------------------------------------------------------------------------
*/

Route::post('/', function (Request $request, JsonRpcServer $server) {
    $input = $request->input();

    // Single (одиночный) запрос
    if (Arr::isAssoc($input)) {
        return $server->handle($input);

    // Batch (множественный) запрос
    } else {
        $response = [];

        foreach ($input as $part) {
            $response[] = $server->handle($part);
        }

        return $response;
    }
});
