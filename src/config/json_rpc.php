<?php

use AlexKudrya\LaravelJsonRpc\JsonRpcAuthPlaceholder;

return [

    /*
    |--------------------------------------------------------------------------
    | JSON-RPC Configuration
    |--------------------------------------------------------------------------
    */

    'api_prefix' => env('JSON_RPC_PREFIX', 'json-rpc/v1'),

    'controllers_root_namespace' => 'App\Http\Controllers\JsonRpc\\',

    'controllers_postfix' => 'Controller',

    'controllers_method_delimiter' => '@',

    'additional_high_level_parameters' => [],

    'auth_required' => env('JSON_RPC_AUTH', false),

    'auth_handler' => [JsonRpcAuthPlaceholder::class, 'handle'],

    'no_auth_methods' => [],
];
