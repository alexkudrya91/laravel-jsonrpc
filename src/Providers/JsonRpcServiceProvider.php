<?php

namespace AlexKudrya\LaravelJsonRpc\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class JsonRpcServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/json_rpc.php' => config_path('json_rpc.php'),
            ]);
        }

        Route::middleware('api')->prefix(config('json_rpc.api_prefix'))->group(function () {
            $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
        });
    }
}
